# Mobile Station Tracking System

This project is a mobile station tracking system that allows you to track the movements of mobile stations in real-time.
The system consists of a backend API built with Spring Boot and a frontend web application built with React.


![Front end view of project](./readme/img/in-house-nav-2.png "Frontend 1")

![Front end view of project](./readme/img/in-house-nav-1.png "Frontend 2")

## Getting Started

To get started with the project, follow these steps:

### First way

1. Clone the repository to your local machine.
2. Navigate to the root directory of the project.
3. Open terminal in the root directory of the project.
4. Run `chmod +x ./run.sh && ./run.sh` to run the project.
5. Open [http://localhost:3000](http://localhost:3000) in your browser to view the application.
6. If for some reason the script doesn't work, follow second way.

### Second way

1. Clone the repository to your local machine.
2. Navigate to the root directory of the project.
3. Build the backend project by running `./gradlew clean && ./gradlew bootJar`.
4. Start the backend using Docker Compose by running `docker-compose up -d`.
5. Navigate to the frontend folder by running `cd front-end`.
6. Install dependencies for the frontend by running `npm install`.
7. Start the frontend by running `npm start`.
8. Open [http://localhost:3000](http://localhost:3000) in your browser to view the application.

## Features

The mobile station tracking system has the following features:

- Real-time tracking of mobile station movements.
- Display of mobile station locations on a map.
- Error radius calculation for each mobile station based on its distance from the nearest base station.

## Technologies Used

The mobile station tracking system uses the following technologies:

- Spring Boot for the backend API.
- React for the frontend web application.
- Docker Compose for containerization and deployment.
- Mockito and JUnit 5 for testing.
- Swagger for API
  documentation: [http://localhost:8081/api/swagger-ui/index.html](http://localhost:8081/api/swagger-ui/index.html)

## Contributing

If you would like to contribute to the project, please follow these steps:

1. Fork the repository to your own GitHub account.
2. Create a new branch for your feature or bug fix.
3. Make your changes and commit them to your branch.
4. Push your changes to your forked repository.
5. Create a pull request from your branch to the main repository.