package az.techlab.repository;

import az.techlab.entity.MobileStation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MobileStationRepository extends JpaRepository<MobileStation, Long> {

}