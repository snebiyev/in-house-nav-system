package az.techlab.repository;

import az.techlab.entity.MobileStationMovement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MobileStationMovementRepository extends JpaRepository<MobileStationMovement, Long> {
    List<MobileStationMovement> findByMobileStationId(Long mobileStationId);

    void deleteAllByMobileStationId(Long mobileStationId);
    void deleteAllByBaseStationId(Long baseStationId);

    List<MobileStationMovement> findByBaseStationId(Long baseStationId);
}
