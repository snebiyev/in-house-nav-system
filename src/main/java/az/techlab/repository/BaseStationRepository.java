package az.techlab.repository;

import az.techlab.entity.BaseStation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BaseStationRepository extends JpaRepository<BaseStation, Long> {
    // EntityGraph is used to load the related entities eagerly and avoid N+1 problem
    @EntityGraph(attributePaths = {"reports","reports.mobileStation"}, type = EntityGraph.EntityGraphType.LOAD)
    Optional<BaseStation> findById(Long id);

    @EntityGraph(attributePaths = {"reports","reports.mobileStation"}, type = EntityGraph.EntityGraphType.LOAD)
    List<BaseStation> findAll();

    @EntityGraph(attributePaths = {"reports","reports.mobileStation"}, type = EntityGraph.EntityGraphType.LOAD)
    Page<BaseStation> findAll(Pageable pageable);

}