package az.techlab.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.Instant;

import static javax.persistence.GenerationType.SEQUENCE;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString(exclude = {"mobileStation", "baseStation"})
@Table(name = "mobile_movement")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class MobileStationMovement {
    //Sequence is used to help Hibernate to make batch inserts
    @Id
    @GeneratedValue(strategy = SEQUENCE, generator = "seqGen")
    @SequenceGenerator(name = "seqGen", sequenceName = "seq", initialValue = 1, allocationSize = 1)
    Long id;

    @ManyToOne
    @JoinColumn(name = "mobile_station_id", nullable = false, updatable = false)
    MobileStation mobileStation;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "base_station_id")
    @JsonIgnore
    BaseStation baseStation;

    @Column(name = "last_known_x")
    Float x;

    @Column(name = "last_known_y")
    Float y;

    @Column(name = "error_radius")
    Float errorRadius;

    @Column(name = "error_code")
    Integer errorCode;

    @Column(name = "error_message")
    String errorMessage;

    @Column(name = "created_at")
    @CreationTimestamp
    Instant createdAt;

    @Column(name = "is_active")
    Boolean isActive;
}
