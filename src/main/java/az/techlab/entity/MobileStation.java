package az.techlab.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@ToString
@Table(name = "mobile_station")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class MobileStation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "last_known_x_coordinate")
    Float lastKnownX;

    @Column(name = "last_known_y_coordinate")
    Float lastKnownY;

    @Column(name = "error_radius")
    Float errorRadius;
    @Column(name = "error_code")
    Integer errorCode;
    @Column(name = "error_message")
    String errorMessage;
}
