package az.techlab.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@EqualsAndHashCode(exclude = "reports")
@ToString
@Table(name = "base_station")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
public class BaseStation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;

    @Column(name = "x_coordinate")
    Float x;

    @Column(name = "y_coordinate")
    Float y;

    @Column(name = "detection_radius")
    Float detectionRadiusInMeters;

    @OneToMany(mappedBy = "baseStation", cascade = CascadeType.DETACH,orphanRemoval = true)
    Set<MobileStationMovement> reports;
}
