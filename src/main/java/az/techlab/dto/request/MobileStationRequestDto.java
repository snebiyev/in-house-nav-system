package az.techlab.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;


@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class MobileStationRequestDto {
    @NotNull(message = "X coordinate is required")
    @DecimalMax(value = "100.0", message = "Value must be less than or equal to {value}")
    @DecimalMin(value = "0.0", message = "Value must be greater than or equal to {value}")
    @Digits(integer = 3, fraction = 2)
    Float lastKnownX;

    @NotNull(message = "Y coordinate is required")
    @DecimalMax(value = "100.0", message = "Value must be less than or equal to {value}")
    @DecimalMin(value = "0.0", message = "Value must be greater than or equal to {value}")
    @Digits(integer = 3, fraction = 2)
    Float lastKnownY;

    Float errorRadius;
    Integer errorCode;
    String errorMessage;
}
