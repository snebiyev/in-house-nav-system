package az.techlab.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseStationRequestDto {
    @NotEmpty(message = "Name is required")
    String name;
    @NotNull(message = "X coordinate is required")
    @DecimalMin(value = "0.0", message = "Value must be greater than {value}")
    @DecimalMax(value = "100.0", message = "Value must be less than or equal to {value}")
    Float x;
    @NotNull(message = "Y coordinate is required")
    @DecimalMin(value = "0.0", message = "Value must be greater than {value}")
    @DecimalMax(value = "100.0", message = "Value must be less than or equal to {value}")
    Float y;
    @NotNull(message = "Detection radius is required")
    @DecimalMin(value = "0.0", inclusive = false, message = "Value must be greater than {value}")
    @DecimalMax(value = "100.0", message = "Value must be less than or equal to {value}")
    Float detectionRadiusInMeters;
}
