package az.techlab.dto.response;

import az.techlab.entity.MobileStationMovement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
@Builder
public class BaseStationResponseDto {
    Long id;
    String name;
    Float x;
    Float y;
    Float detectionRadiusInMeters;
    Set<MobileStationMovement> reports;
}
