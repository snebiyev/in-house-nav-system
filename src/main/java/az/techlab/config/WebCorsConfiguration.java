package az.techlab.config;

import lombok.experimental.FieldDefaults;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;

@Component
@Order(HIGHEST_PRECEDENCE)
@FieldDefaults(level = lombok.AccessLevel.PRIVATE)
public class WebCorsConfiguration implements Filter {
    List<String> listDomainsAllowed;
    boolean isAnyDomainAllowed = true;
    static final String ACAO = "Access-Control-Allow-Origin";


    @Override
    public void doFilter(ServletRequest req, ServletResponse res,
                         FilterChain chain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) res;
        HttpServletRequest request = (HttpServletRequest) req;

        validateRequestOriginInDomainsAllowed(response, request);
        response.setHeader("Access-Control-Allow-Methods",
                "GET, PUT, POST, PATCH, DELETE, HEAD, OPTIONS");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers",
                "X-Requested-With, Content-Type, Accept, Origin, Authorization");
        chain.doFilter(req, res);
    }

    private void validateRequestOriginInDomainsAllowed(
            final HttpServletResponse res, final HttpServletRequest req) {
        final String originRequest = req.getHeader("origin");

        String anyDomainAllowed = "*";
        if (listDomainsAllowed == null) {
            final String domainsAccessControlAllowOrigin = getDomainsAccessControlAllowOrigin();
            final String delimiter = ",";
            if (anyDomainAllowed.equals(domainsAccessControlAllowOrigin)) {
                isAnyDomainAllowed = true;
            }
            final String[] domainsAllowed = StringUtils.tokenizeToStringArray(
                    domainsAccessControlAllowOrigin, delimiter);
            listDomainsAllowed = new ArrayList<>(
                    Arrays.asList(domainsAllowed));
        }
        if (isAnyDomainAllowed) {
            if (!res.containsHeader(ACAO)) {
                res.addHeader(ACAO, "*");
            } else {
                res.setHeader(ACAO, "*");
            }
        } else if (listDomainsAllowed.contains(originRequest)
                && org.apache.commons.validator.routines.UrlValidator
                .getInstance().isValid(originRequest)) {
            res.addHeader(ACAO, originRequest);
        }
    }

    private String getDomainsAccessControlAllowOrigin() {
        return "*";
    }
}