package az.techlab.service;

import az.techlab.dto.request.BaseStationRequestDto;
import az.techlab.dto.response.BaseStationResponseDto;
import az.techlab.entity.BaseStation;
import az.techlab.event.BaseStationOfflineEvent;
import az.techlab.event.BaseStationOnlineEvent;
import az.techlab.exception.NotFoundException;
import az.techlab.repository.BaseStationRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequiredArgsConstructor
public class BaseStationService {
    BaseStationRepository repository;
    BaseStationOfflineEvent baseStationOfflineEvent;
    BaseStationOnlineEvent baseStationOnlineEvent;
    ModelMapper modelMapper;


    public BaseStationResponseDto save(BaseStationRequestDto requestDto) {
        BaseStation data = modelMapper.map(requestDto, BaseStation.class);
        log.info("Base station to save: {}", data);
        BaseStation saved = repository.save(data);
        log.info("Base station saved: {}", saved);
        baseStationOnlineEvent.publishEvent(saved.getId(), saved.getX(), saved.getY(), saved.getDetectionRadiusInMeters());
        return modelMapper.map(saved, BaseStationResponseDto.class);
    }

    public BaseStationResponseDto update(Long id, BaseStationRequestDto requestDto) {
        // Check if base station exists
        if (!repository.existsById(id)) {
            log.trace("Base station with id {} not found", id);
            throw new NotFoundException(String.format("Base station with id %s not found", id));
        }
        BaseStation baseStation = modelMapper.map(requestDto, BaseStation.class);
        baseStation.setId(id);
        log.info("Base station to update: {}", baseStation);
        BaseStation saved = repository.save(baseStation);
        log.info("Base station updated: {}", saved);
        return modelMapper.map(saved, BaseStationResponseDto.class);
    }


    public BaseStationResponseDto findById(Long id) {
        Optional<BaseStation> optionalBaseStation = repository.findById(id);
        if (optionalBaseStation.isPresent()) {
            return modelMapper.map(optionalBaseStation.get(), BaseStationResponseDto.class);
        } else {
            throw new NotFoundException(String.format("Base station with id %s not found", id));
        }
    }

    public List<BaseStation> findAll() {
        return repository.findAll();
    }

    public void deleteById(Long id) {
        checkIfBaseStationExists(id);
        baseStationOfflineEvent.publishEvent(id);
        log.info("Base station with id {} deleted", id);
    }

    public void deleteByIdSilently(Long id) {
        repository.deleteById(id);
        log.info("Base station with id {} deleted", id);
    }

    private void checkIfBaseStationExists(Long id) {
        if (!repository.existsById(id)) {
            log.trace("Base station with id {} not found", id);
            throw new NotFoundException(String.format("Base station with id %s not found", id));
        }
    }

    public Page<BaseStationResponseDto> findAll(Pageable pageable) {
        return repository.findAll(pageable)
                .map(baseStation -> modelMapper.map(baseStation, BaseStationResponseDto.class));
    }
}
