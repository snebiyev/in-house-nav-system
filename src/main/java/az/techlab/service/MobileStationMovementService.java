package az.techlab.service;

import az.techlab.entity.MobileStationMovement;
import az.techlab.repository.MobileStationMovementRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequiredArgsConstructor
public class MobileStationMovementService {
    MobileStationMovementRepository repository;

    @Transactional
    public void save(MobileStationMovement data) {
        repository.deleteAllByMobileStationId(data.getMobileStation().getId());
        log.info("Mobile station movement to save: {}", data);
        MobileStationMovement saved = repository.save(data);
        log.info("Mobile station movement saved: {}", saved);
    }

    public void deleteAllByMobileStationId(Long mobileStationId) {
        log.info("Mobile station movements to delete by mobile station id: {}", mobileStationId);
        repository.deleteAllByMobileStationId(mobileStationId);
        log.info("Mobile station movements deleted");
    }

    public void deleteAllByBaseStationId(Long baseStationId) {
        log.info("Mobile station movements to delete by base station id: {}", baseStationId);
        repository.deleteAllByBaseStationId(baseStationId);
        log.info("Mobile station movements deleted");
    }

    @Transactional
    public void saveAll(List<MobileStationMovement> mobileStationMovements) {
        repository.deleteAllByMobileStationId(mobileStationMovements.get(0).getMobileStation().getId());
        log.info("Mobile station movements to save: {}", mobileStationMovements);
        List<MobileStationMovement> saved = repository.saveAll(mobileStationMovements);
        log.info("Mobile station movements saved: {}", saved);
    }

    public List<MobileStationMovement> findByBaseStationId(Long baseStationId) {
        log.info("Mobile station movements to find by base station id: {}", baseStationId);
        List<MobileStationMovement> mobileStationMovements = repository.findByBaseStationId(baseStationId);
        log.info("Mobile station movements found: {}", mobileStationMovements);
        return mobileStationMovements;
    }
}
