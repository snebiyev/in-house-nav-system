package az.techlab.service;

import az.techlab.dto.request.MobileStationRequestDto;
import az.techlab.entity.BaseStation;
import az.techlab.entity.MobileStation;
import az.techlab.entity.MobileStationMovement;
import az.techlab.event.*;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequiredArgsConstructor
public class EventRegistrationService {
    BaseStationService baseStationService;
    MobileStationService mobileStationService;
    MobileStationMovementService mobileStationMovementService;
    MobileStationMovedEvent mobileStationMovedEvent;
    ModelMapper modelMapper;

    /*
    I know guys, this is not the best way to do it, but I'm running out of time.
     */

    @EventListener
    @Transactional
    public void handleBaseStationOnlineEvent(BaseStationOnlineEventObject eventObj) {
        log.info("Base station {} is online", eventObj.getId());
        List<MobileStation> mobileStations = mobileStationService.findAll();
        List<MobileStation> mobileStationsInRange = new ArrayList<>();
        List<MobileStationMovement> mobileStationMovements = new ArrayList<>();
        mobileStations.forEach(station -> {
            double distance = calculateDistance(eventObj.getX(), eventObj.getY(), station.getLastKnownX(), station.getLastKnownY());
            if (distance <= eventObj.getDetectionRadiusInMeters()) {
                mobileStationMovements.add(MobileStationMovement
                        .builder()
                        .mobileStation(station)
                        .baseStation(BaseStation.builder().id(eventObj.getId()).build())
                        .x(station.getLastKnownX())
                        .y(station.getLastKnownY())
                        .build());
                station.setErrorRadius(null);
                station.setErrorMessage(null);
                station.setErrorCode(null);
                mobileStationsInRange.add(station);
                log.info("Mobile station {} is in range of base station {}", station.getId(), eventObj.getId());
            }
        });
        if (mobileStationMovements.size() > 0)
            mobileStationMovementService.saveAll(mobileStationMovements);
        if (mobileStationsInRange.size() > 0)
            mobileStationService.saveAll(mobileStationsInRange);
    }

    @EventListener
    @Transactional
    public void handleBaseStationOfflineEvent(BaseStationOfflineEventObject eventObject) {
        log.info("Base station {} is offline", eventObject.getId());
        List<MobileStationMovement> movements = mobileStationMovementService.findByBaseStationId(eventObject.getId());
        mobileStationMovementService.deleteAllByBaseStationId(eventObject.getId());
        baseStationService.deleteByIdSilently(eventObject.getId());
        // Re-publish mobile station moved events
        movements.forEach(move -> {
            mobileStationMovedEvent.publishEvent(move.getMobileStation().getId(), move.getX(), move.getY());
        });

    }


    @EventListener
    @Transactional
    public void handleMobileStationOfflineEvent(MobileStationOfflineEventObject eventObject) {
        log.info("Mobile station {} is offline", eventObject.getMobileStationId());
        mobileStationMovementService.deleteAllByMobileStationId(eventObject.getMobileStationId());
        mobileStationService.deleteByIdSilently(eventObject.getMobileStationId());

    }


    @EventListener
    // Here a lot of mess is happening :)
    public void handleMobileStationMovedEvent(MobileStationMovedEventObject eventObject) {
        // Here we can use EventSource to store all movements of mobile stations
        List<BaseStation> baseStations = baseStationService.findAll();
        List<MobileStationMovement> mobileStationMovements = new ArrayList<>();
        Integer errorCode = null;
        String errorMessage = null;
        Float errorRadius = null;
        double minErrorRadius = Double.MAX_VALUE;
        MobileStation mobileStation = MobileStation.builder().id(eventObject.getMobileStationId()).build();

        for (BaseStation baseStation : baseStations) {
            double distance = calculateDistance(baseStation.getX(), baseStation.getY(), eventObject.getX(), eventObject.getY());
            if (distance <= baseStation.getDetectionRadiusInMeters()) {
                mobileStationMovements.add(MobileStationMovement
                        .builder()
                        .mobileStation(mobileStation)
                        .baseStation(baseStation)
                        .x(eventObject.getX())
                        .y(eventObject.getY())
                        .build());
                log.info("Mobile station {} is in range of base station {}", eventObject.getMobileStationId(), baseStation.getId());
            } else {
                double range = distance - baseStation.getDetectionRadiusInMeters();
                if (minErrorRadius > range) {
                    minErrorRadius = range;
                }
            }
        }
        // If mobile station is not in range of any base station, then we should add error message to the mobile station
        if (mobileStationMovements.size() == 0) {
            log.info("Mobile station {} is not in range of any base station", eventObject.getMobileStationId());
            log.info("Error radius is {}", minErrorRadius);
            errorCode = 1;
            errorMessage = "Mobile station is not in range of any base station";
            errorRadius = (float) minErrorRadius;
            MobileStationMovement caught = MobileStationMovement
                    .builder()
                    .mobileStation(mobileStation)
                    .x(eventObject.getX())
                    .y(eventObject.getY())
                    .errorCode(errorCode)
                    .errorMessage(errorMessage)
                    .errorRadius(errorRadius)
                    .build();
            mobileStationMovementService.save(caught);
            // Save error message to the mobile station

        } else {
            log.info("Mobile station {} is in range of {} base stations", eventObject.getMobileStationId(), mobileStationMovements.size());
            //Save all mobile station movements in one transaction
            mobileStationMovementService.saveAll(mobileStationMovements);
        }
        MobileStation ms = MobileStation.builder()
                .errorCode(errorCode)
                .errorMessage(errorMessage)
                .errorRadius(errorRadius)
                .lastKnownX(eventObject.getX())
                .lastKnownY(eventObject.getY())
                .build();
        mobileStationService.update(eventObject.getMobileStationId(), modelMapper.map(ms, MobileStationRequestDto.class));
    }


    private double calculateDistance(float baseX, float baseY, float mobileX, float mobileY) {
        return Math.sqrt(Math.pow(baseX - mobileX, 2) + Math.pow(baseY - mobileY, 2));
    }

}
