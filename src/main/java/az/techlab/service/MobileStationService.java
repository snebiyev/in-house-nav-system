package az.techlab.service;

import az.techlab.dto.request.MobileStationRequestDto;
import az.techlab.dto.response.MobileStationResponseDto;
import az.techlab.entity.MobileStation;
import az.techlab.event.MobileStationMovedEvent;
import az.techlab.event.MobileStationOfflineEvent;
import az.techlab.exception.NotFoundException;
import az.techlab.repository.MobileStationRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequiredArgsConstructor
public class MobileStationService {
    MobileStationRepository repository;
    MobileStationMovedEvent mobileStationMovedEvent;
    MobileStationOfflineEvent mobileStationOfflineEvent;
    ModelMapper modelMapper;

    public MobileStationResponseDto save(MobileStationRequestDto requestDto) {
        MobileStation data = modelMapper.map(requestDto, MobileStation.class);
        log.info("Mobile station to save: {}", data);
        MobileStation saved = repository.save(data);
        log.info("Mobile station saved: {}", saved);
        MobileStationResponseDto dto = modelMapper.map(saved, MobileStationResponseDto.class);
        mobileStationMovedEvent.publishEvent(dto.getId(), requestDto.getLastKnownX(), requestDto.getLastKnownY());
        return dto;
    }

    public void saveAll(List<MobileStation> mobileStations) {
        repository.saveAll(mobileStations);
    }

    public MobileStationResponseDto update(Long id, MobileStationRequestDto requestDto) {
        // Check if mobile station exists
        if (!repository.existsById(id)) {
            log.trace("Mobile station with id {} not found", id);
            throw new NotFoundException(String.format("Mobile station with id %s not found", id));
        }
        MobileStation mobileStation = modelMapper.map(requestDto, MobileStation.class);
        mobileStation.setId(id);
        log.info("Mobile station to update: {}", mobileStation);
        MobileStation saved = repository.save(mobileStation);
        log.info("Mobile station updated: {}", saved);
        return modelMapper.map(saved, MobileStationResponseDto.class);
    }


    public MobileStationResponseDto findById(Long id) {
        Optional<MobileStation> optionalMobileStation = repository.findById(id);
        if (optionalMobileStation.isPresent()) {
            return modelMapper.map(optionalMobileStation.get(), MobileStationResponseDto.class);
        } else {
            throw new NotFoundException(String.format("Mobile station with id %s not found", id));
        }
    }

    public void deleteById(Long id) {
        checkIfMobileStationExists(id);
        mobileStationOfflineEvent.publishEvent(id);
        log.info("Mobile station with id {} deleted", id);
    }

    public void deleteByIdSilently(Long id) {
        repository.deleteById(id);
        log.info("Mobile station with id {} deleted", id);
    }

    private void checkIfMobileStationExists(Long id) {
        if (!repository.existsById(id)) {
            log.trace("Mobile station with id {} not found", id);
            throw new NotFoundException(String.format("Mobile station with id %s not found", id));
        }
    }

    public Page<MobileStationResponseDto> findAll(Pageable pageable) {
        return repository.findAll(pageable)
                .map(mobileStation -> modelMapper.map(mobileStation, MobileStationResponseDto.class));
    }

    public List<MobileStation> findAll() {
        return repository.findAll();
    }

    public boolean move(Long id, MobileStationRequestDto requestDto) {
        checkIfMobileStationExists(id);
        mobileStationMovedEvent.publishEvent(id, requestDto.getLastKnownX(), requestDto.getLastKnownY());
        return true;
    }
}
