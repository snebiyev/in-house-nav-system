package az.techlab.controller;


import az.techlab.dto.request.BaseStationRequestDto;
import az.techlab.dto.response.BaseStationResponseDto;
import az.techlab.service.BaseStationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/base-stations")
@RequiredArgsConstructor
@FieldDefaults(level = lombok.AccessLevel.PRIVATE, makeFinal = true)
public class BaseStationController {

    BaseStationService service;

    @Operation(summary = "Create new Base Station", description = "Create new Base Station")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Successful operation")})
    @PostMapping("/")
    public ResponseEntity<BaseStationResponseDto> save(@Validated @RequestBody BaseStationRequestDto requestDto) {
        return new ResponseEntity<>(service.save(requestDto), HttpStatus.CREATED);
    }

    @Operation(summary = "Get all Base Stations", description = "Get all Base Stations")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "404", description = "No data found based on given filter")})
    @GetMapping("/")
    public ResponseEntity<Page<BaseStationResponseDto>> getAll(@PageableDefault Pageable pageable) {
        return new ResponseEntity<>(service.findAll(pageable), HttpStatus.OK);
    }

    @Operation(summary = "Get a Base Station by id", description = "Get a Base Station by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "404", description = "Base Station not found")})
    @GetMapping("/{id}")
    public ResponseEntity<BaseStationResponseDto> getById(@PathVariable Long id) {
        return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    }

    @Operation(summary = "Update a Base Station by id", description = "Update a Base Station by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "404", description = "Base Station not found")})
    @PutMapping("/{id}")
    public ResponseEntity<BaseStationResponseDto> updateById(@PathVariable Long id, @Validated @RequestBody BaseStationRequestDto requestDto) {
        return new ResponseEntity<>(service.update(id, requestDto), HttpStatus.OK);
    }

    @Operation(summary = "Delete a Base Station by id", description = "Delete a Base Station by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "404", description = "Base Station not found")})
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Long id) {
        service.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}