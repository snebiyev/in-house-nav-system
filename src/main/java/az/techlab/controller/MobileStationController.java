package az.techlab.controller;

import az.techlab.dto.request.MobileStationRequestDto;
import az.techlab.dto.response.MobileStationResponseDto;
import az.techlab.service.MobileStationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/mobile-stations")
@RequiredArgsConstructor
@FieldDefaults(level = lombok.AccessLevel.PRIVATE, makeFinal = true)
public class MobileStationController {
    MobileStationService service;

    @Operation(summary = "Create new Mobile Station", description = "Create new Mobile Station")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation")})
    @PostMapping("/")
    public ResponseEntity<MobileStationResponseDto> save(@Validated @RequestBody MobileStationRequestDto requestDto) {
        return new ResponseEntity<>(service.save(requestDto), HttpStatus.OK);
    }

    @Operation(summary = "Get all Mobile Stations", description = "Get all Mobile Stations")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "404", description = "No data found based on given filter")})
    @GetMapping("/")
    public ResponseEntity<Page<MobileStationResponseDto>> getAll(@PageableDefault Pageable pageable) {
        return new ResponseEntity<>(service.findAll(pageable), HttpStatus.OK);
    }

    @Operation(summary = "Get a Mobile Station by id", description = "Get a Mobile Station by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "404", description = "Mobile Station not found")})
    @GetMapping("/location/{id}")
    public ResponseEntity<MobileStationResponseDto> getById(@PathVariable Long id) {
        return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
    }

    @Operation(summary = "Update a Mobile Station by id", description = "Update a Base Mobile by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "404", description = "Mobile Station not found")})
    @PutMapping("/{id}")
    public ResponseEntity<MobileStationResponseDto> updateById(@PathVariable Long id,@Validated @RequestBody MobileStationRequestDto requestDto) {
        return new ResponseEntity<>(service.update(id, requestDto), HttpStatus.OK);
    }

    @Operation(summary = "Move a Mobile Station by id", description = "Move a Mobile Station by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "404", description = "Mobile Station not found")})
    @PutMapping("/{id}/move")
    public ResponseEntity<Boolean> moveById(@PathVariable Long id,@Validated @RequestBody MobileStationRequestDto requestDto) {
        return new ResponseEntity<>(service.move(id, requestDto), HttpStatus.OK);
    }

    @Operation(summary = "Delete a Mobile Station by id", description = "Delete a Mobile Station by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "404", description = "Mobile Station not found")})
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteById(@PathVariable Long id) {
        service.deleteById(id);
        return ResponseEntity.noContent().build();
    }


}
