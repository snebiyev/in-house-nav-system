package az.techlab.exception;


public class NotFoundException extends RuntimeException {

    public NotFoundException(final String message) {
        super(message);
    }

    public NotFoundException(final String message, Throwable ex) {
        super(message, ex);
    }
}
