package az.techlab.exception;

public class InterruptedRequestException extends RuntimeException{
    public InterruptedRequestException(String message) {
        super(message);
    }
}
