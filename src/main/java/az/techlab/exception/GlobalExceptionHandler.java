package az.techlab.exception;


import az.techlab.exception.model.ErrorResponse;
import az.techlab.exception.model.ValidationError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;

import javax.naming.ServiceUnavailableException;
import javax.naming.SizeLimitExceededException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.net.UnknownHostException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ResponseStatus(value = HttpStatus.SERVICE_UNAVAILABLE)
    @ExceptionHandler(value = ServiceUnavailableException.class)
    public ResponseEntity<ErrorResponse> serviceUnavailableHandler(ServiceUnavailableException e) {
        log.error("ServiceUnavailableException: ", e);
        return buildErrorResponse(e, HttpStatus.SERVICE_UNAVAILABLE);
    }

    @ExceptionHandler(value = HttpClientErrorException.BadRequest.class)
    public ResponseEntity<ErrorResponse> badRequestHandler(HttpClientErrorException.BadRequest e) {
        log.error("BadRequestException: ", e);
        return buildErrorResponse(e, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = CustomBadRequestException.class)
    public ResponseEntity<ErrorResponse> badRequestHandler(CustomBadRequestException e) {
        log.error("CustomBadRequestException: ", e);
        return buildErrorResponse(e, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = HttpMessageNotReadableException.class)
    public ResponseEntity<ErrorResponse> badRequestHandler(HttpMessageNotReadableException e) {
        log.error("HttpMessageNotReadableException: ", e);
        return buildErrorResponse(e, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = CustomException.class)
    public ResponseEntity<ErrorResponse> badRequestHandler(CustomException e) {
        log.trace("CustomException: ", e);
        return buildErrorResponse(e, e.getHttpStatus());
    }


    @ExceptionHandler(value = ForbiddenRequestException.class)
    public ResponseEntity<ErrorResponse> forbiddenRequestHandler(ForbiddenRequestException e) {
        log.trace("ForbiddenRequestException: ", e);
        return buildErrorResponse(e, HttpStatus.FORBIDDEN);
    }


    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public Map<String, String> handleValidations(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        log.trace("MethodArgumentNotValidException: ", ex);
        return errors;
    }


    @ExceptionHandler(value = NotFoundException.class)
    public ResponseEntity<ErrorResponse> resourceNotFoundHandler(NotFoundException e) {
        log.trace("NotFoundException: ", e);
        return buildErrorResponse(e, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = ResourceNotFoundException.class)
    public ResponseEntity<ErrorResponse> resourceNotFoundHandler(ResourceNotFoundException e) {
        log.trace("ResourceNotFoundException: ", e);
        return buildErrorResponse(e, HttpStatus.NOT_FOUND);
    }


    @ExceptionHandler(value = SQLIntegrityConstraintViolationException.class)
    public ResponseEntity<ErrorResponse> sqlConstraintViolation(SQLIntegrityConstraintViolationException ex) {
        log.trace("SQLIntegrityConstraintViolationException: ", ex);
        return buildErrorResponse(ex, HttpStatus.BAD_REQUEST);
    }


    //    @ExceptionHandler(value = UserAlreadyRegisteredException.class)
//    public ResponseEntity<ErrorResponse> userAlreadyExistsHandler(UserAlreadyRegisteredException e) {
//        return buildErrorResponse(e, HttpStatus.BAD_REQUEST);
//    }
    @ExceptionHandler(value = PropertyReferenceException.class)
    public ResponseEntity<ErrorResponse> propertyReferenceError(PropertyReferenceException e) {
        log.trace("PropertyReferenceException: ", e);
        return buildErrorResponse(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = InterruptedRequestException.class)
    public ResponseEntity<ErrorResponse> requestWasInterrupted(InterruptedRequestException e) {
        log.trace("InterruptedRequestException: ", e);
        return buildErrorResponse(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = UnknownHostException.class)
    public ResponseEntity<ErrorResponse> unknownHost(UnknownHostException e) {
        log.trace("UnknownHostException: ", e);
        return requestWasInterrupted(new InterruptedRequestException(e.getMessage()));
    }


    @ExceptionHandler(value = SizeLimitExceededException.class)
    public ResponseEntity<ErrorResponse> fileSizeExceeded(SizeLimitExceededException s) {
        log.trace("SizeLimitExceededException: ", s);
        return badRequestHandler(new CustomBadRequestException(s.getMessage()));
    }


    @ExceptionHandler(value = DataIntegrityViolationException.class)
    public ResponseEntity<ErrorResponse> integrityViolation(DataIntegrityViolationException e) {
        log.trace("DataIntegrityViolationException: ", e);
        return buildErrorResponse(new CustomBadRequestException("Unique Constraint or Related violated"), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = EmptyResultDataAccessException.class)
    public ResponseEntity<ErrorResponse> emptyResultDataAccessException(EmptyResultDataAccessException e) {
        log.trace("EmptyResultDataAccessException: ", e);
        return buildErrorResponse(new CustomBadRequestException("Entity does not exist"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Constraint violation exception handler
     *
     * @param ex ConstraintViolationException
     * @return List<ValidationError> - list of ValidationError built
     * from set of ConstraintViolation
     */
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(ConstraintViolationException.class)
    public List<ValidationError> handleConstraintViolation(ConstraintViolationException ex) {
        log.trace("ConstraintViolationException: ", ex);
        return buildValidationErrors(ex.getConstraintViolations());
    }

    /**
     * Build list of ValidationError from set of ConstraintViolation
     *
     * @param violations Set<ConstraintViolation<?>> - Set
     *                   of parameterized ConstraintViolations
     * @return List<ValidationError> - list of validation errors
     */
    private List<ValidationError> buildValidationErrors(Set<ConstraintViolation<?>> violations) {
        return violations.
                stream().
                map(violation ->
                        ValidationError.builder().
                                field(
                                        Objects.requireNonNull(StreamSupport.stream(
                                                                violation.getPropertyPath().spliterator(), false).
                                                        reduce((first, second) -> second).
                                                        orElse(null)).
                                                toString()
                                ).
                                error(violation.getMessage()).
                                build()).
                collect(Collectors.toList());
    }

    private ResponseEntity<ErrorResponse> buildErrorResponse(Exception e, HttpStatus httpStatus) {
        ErrorResponse response = new ErrorResponse();
        response.setCode(httpStatus.value());
        response.setStatus(httpStatus.toString());
        response.setMessage(e.getMessage());
        response.setTimestamp(LocalDateTime.now().toString());
        return new ResponseEntity<>(response, httpStatus);
    }
}
