package az.techlab.event;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = lombok.AccessLevel.PRIVATE)
public class MobileStationOfflineEvent {
    ApplicationEventPublisher publisher;

    public void publishEvent(Long id) {
        log.info("Publishing event: mobile station {} is offline", id);
        publisher.publishEvent(new MobileStationOfflineEventObject(this, id));
    }
}
