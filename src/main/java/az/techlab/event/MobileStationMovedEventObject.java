package az.techlab.event;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Setter
@Getter
public class MobileStationMovedEventObject extends ApplicationEvent {
    public Long mobileStationId;
    public Float x;
    public Float y;
    public MobileStationMovedEventObject(MobileStationMovedEvent moveEvent, Long id, Float x, Float y) {
        super(moveEvent);
        this.mobileStationId = id;
        this.x = x;
        this.y = y;
    }
}
