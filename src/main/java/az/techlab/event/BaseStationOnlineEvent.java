package az.techlab.event;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = lombok.AccessLevel.PRIVATE)
public class BaseStationOnlineEvent {
    ApplicationEventPublisher publisher;

    public void publishEvent(Long id, Float x, Float y, Float detectionRadiusInMeters) {
        log.info("Publishing event: base station {} is online", id);
        publisher.publishEvent(new BaseStationOnlineEventObject(this, id, x, y, detectionRadiusInMeters));
    }
}
