package az.techlab.event;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = lombok.AccessLevel.PRIVATE)
public class BaseStationOfflineEvent {
    ApplicationEventPublisher publisher;

    public void publishEvent(Long id) {
        log.info("Publishing event: base station {} is offline", id);
        publisher.publishEvent(new BaseStationOfflineEventObject(this, id));
    }
}
