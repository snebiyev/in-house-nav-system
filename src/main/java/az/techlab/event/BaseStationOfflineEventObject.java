package az.techlab.event;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;
@Setter
@Getter
public class BaseStationOfflineEventObject extends ApplicationEvent {
    public Long id;

    public BaseStationOfflineEventObject(BaseStationOfflineEvent offlineEvent, Long id) {
        super(offlineEvent);
        this.id = id;
    }
}