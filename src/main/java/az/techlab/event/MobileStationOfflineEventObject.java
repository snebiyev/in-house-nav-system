package az.techlab.event;

import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Setter
@Getter
public class MobileStationOfflineEventObject extends ApplicationEvent {
    public Long mobileStationId;

    public MobileStationOfflineEventObject(MobileStationOfflineEvent offlineEvent, Long id) {
        super(offlineEvent);
        this.mobileStationId = id;
    }
}
