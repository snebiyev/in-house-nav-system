package az.techlab.event;

import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
@FieldDefaults(makeFinal = true, level = lombok.AccessLevel.PRIVATE)
public class MobileStationMovedEvent {
    ApplicationEventPublisher publisher;

    public void publishEvent(Long id, Float x, Float y) {
        log.info("Publishing event: mobile station {} moved to x {}, y {}", id, x, y);
        publisher.publishEvent(new MobileStationMovedEventObject(this, id, x, y));
    }
}
