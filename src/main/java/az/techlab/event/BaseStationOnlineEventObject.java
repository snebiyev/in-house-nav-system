package az.techlab.event;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Setter
@Getter
public class BaseStationOnlineEventObject extends ApplicationEvent {
    public Long id;
    public Float x;
    public Float y;
    public Float detectionRadiusInMeters;

    public BaseStationOnlineEventObject(BaseStationOnlineEvent event, Long id, Float x, Float y, Float detectionRadiusInMeters) {
        super(event);
        this.id = id;
        this.x = x;
        this.y = y;
        this.detectionRadiusInMeters = detectionRadiusInMeters;
    }
}