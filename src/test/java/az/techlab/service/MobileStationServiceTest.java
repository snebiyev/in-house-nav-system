package az.techlab.service;

import az.techlab.dto.request.MobileStationRequestDto;
import az.techlab.dto.response.MobileStationResponseDto;
import az.techlab.entity.MobileStation;
import az.techlab.event.MobileStationMovedEvent;
import az.techlab.event.MobileStationOfflineEvent;
import az.techlab.exception.NotFoundException;
import az.techlab.repository.MobileStationRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.*;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("Mobile station service")
class MobileStationServiceTest {
    @Mock
    MobileStationRepository repository;
    @Mock
    MobileStationMovedEvent mobileStationMovedEvent;
    @Mock
    MobileStationOfflineEvent mobileStationOfflineEvent;
    @Mock
    ModelMapper modelMapper;

    @InjectMocks
    MobileStationService service;

    @Test
    @DisplayName("Should throw NotFoundException when the id does not exist")
    void moveWhenIdDoesNotExistThenThrowNotFoundException() {
        Long id = 1L;
        MobileStationRequestDto requestDto = new MobileStationRequestDto();
        requestDto.setLastKnownX(10.0f);
        requestDto.setLastKnownY(20.0f);

        when(repository.existsById(id)).thenReturn(false);

        assertThrows(
                NotFoundException.class,
                () -> service.move(id, requestDto),
                "Expected NotFoundException to be thrown");

        verify(repository, times(1)).existsById(id);
        verifyNoMoreInteractions(
                repository, mobileStationMovedEvent, mobileStationOfflineEvent, modelMapper);
    }

    @Test
    @DisplayName("Should move the mobile station when the id exists")
    void moveWhenIdExists() {
        Long id = 1L;
        MobileStationRequestDto requestDto = new MobileStationRequestDto();
        requestDto.setLastKnownX(10.0f);
        requestDto.setLastKnownY(20.0f);

        // Mocking the repository to return an optional containing a mobile station
        when(repository.existsById(id)).thenReturn(true);
        // Calling the move method
        boolean result = service.move(id, requestDto);

        // Verifying that the mobile station moved event was published
        verify(mobileStationMovedEvent, times(1))
                .publishEvent(
                        eq(id), eq(requestDto.getLastKnownX()), eq(requestDto.getLastKnownY()));

        // Asserting that the result is true
        assertTrue(result);
    }

    @Test
    @DisplayName("Should return all mobile stations")
    void findAllMobileStations() { // create pageable object
        Pageable pageable = PageRequest.of(0, 10, Sort.by("id"));

        // create a list of mobile stations
        List<MobileStation> mobileStations = new ArrayList<>();
        mobileStations.add(new MobileStation(1L, 10.0f, 20.0f, 5.0f, 0, ""));
        mobileStations.add(new MobileStation(2L, 30.0f, 40.0f, 5.0f, 0, ""));
        mobileStations.add(new MobileStation(3L, 50.0f, 60.0f, 5.0f, 0, ""));

        // create a page object
        Page<MobileStation> mobileStationPage =
                new PageImpl<>(mobileStations, pageable, mobileStations.size());

        // mock the repository method
        when(repository.findAll(pageable)).thenReturn(mobileStationPage);

        // create a list of expected mobile station response dtos
        List<MobileStationResponseDto> expectedMobileStationResponseDtos = new ArrayList<>();
        expectedMobileStationResponseDtos.add(
                new MobileStationResponseDto(1L, 10.0f, 20.0f, 5.0f, 0, ""));
        expectedMobileStationResponseDtos.add(
                new MobileStationResponseDto(2L, 30.0f, 40.0f, 5.0f, 0, ""));
        expectedMobileStationResponseDtos.add(
                new MobileStationResponseDto(3L, 50.0f, 60.0f, 5.0f, 0, ""));

        // mock the model mapper method
        when(modelMapper.map(any(MobileStation.class), eq(MobileStationResponseDto.class)))
                .thenAnswer(
                        invocation -> {
                            MobileStation mobileStation = invocation.getArgument(0);
                            return new MobileStationResponseDto(
                                    mobileStation.getId(),
                                    mobileStation.getLastKnownX(),
                                    mobileStation.getLastKnownY(),
                                    mobileStation.getErrorRadius(),
                                    mobileStation.getErrorCode(),
                                    mobileStation.getErrorMessage());
                        });

        // call the method under test
        Page<MobileStationResponseDto> result = service.findAll(pageable);

        // verify the repository method was called once
        verify(repository, times(1)).findAll(pageable);

        // verify the model mapper method was called three times
        verify(modelMapper, times(3))
                .map(any(MobileStation.class), eq(MobileStationResponseDto.class));

        // assert that the result matches the expected mobile station response dtos
        assertEquals(expectedMobileStationResponseDtos, result.getContent());
    }

    @Test
    @DisplayName("Should return an empty page when there are no mobile stations in the repository")
    void findAllWithPageableReturnsEmptyPageWhenNoMobileStations() {
        int pageNumber = 0;
        int pageSize = 10;
        String sortBy = "id";
        String sortDirection = "asc";
        Pageable pageable =
                PageRequest.of(
                        pageNumber,
                        pageSize,
                        Sort.by(Sort.Direction.fromString(sortDirection), sortBy));
        List<MobileStation> mobileStations = new ArrayList<>();
        Page<MobileStation> page = new PageImpl<>(mobileStations, pageable, 0L);
        when(repository.findAll(pageable)).thenReturn(page);

        Page<MobileStationResponseDto> result = service.findAll(pageable);

        assertEquals(0, result.getTotalElements());
        assertEquals(0, result.getContent().size());
        verify(repository, times(1)).findAll(pageable);
    }

    @Test
    @DisplayName(
            "Should return a page of MobileStationResponseDto when findAll is called with pageable")
    void findAllWithPageableReturnsPageOfMobileStationResponseDto() { // create pageable object
        Pageable pageable = PageRequest.of(0, 10);

        // create a list of MobileStation objects
        List<MobileStation> mobileStations = new ArrayList<>();
        mobileStations.add(new MobileStation(1L, 10.0f, 20.0f, 5.0f, 0, "No error"));
        mobileStations.add(new MobileStation(2L, 30.0f, 40.0f, 5.0f, 0, "No error"));

        // mock the repository to return the list of MobileStation objects
        when(repository.findAll(pageable)).thenReturn(new PageImpl<>(mobileStations));

        // mock the modelMapper to map MobileStation objects to MobileStationResponseDto objects
        when(modelMapper.map(any(MobileStation.class), eq(MobileStationResponseDto.class)))
                .thenAnswer(
                        invocation -> {
                            MobileStation mobileStation = invocation.getArgument(0);
                            return new MobileStationResponseDto(
                                    mobileStation.getId(),
                                    mobileStation.getLastKnownX(),
                                    mobileStation.getLastKnownY(),
                                    mobileStation.getErrorRadius(),
                                    mobileStation.getErrorCode(),
                                    mobileStation.getErrorMessage());
                        });

        // call the method under test
        Page<MobileStationResponseDto> result = service.findAll(pageable);

        // verify that the repository method was called once with the correct pageable object
        verify(repository, times(1)).findAll(pageable);

        // verify that the modelMapper method was called twice (once for each MobileStation object)
        verify(modelMapper, times(2))
                .map(any(MobileStation.class), eq(MobileStationResponseDto.class));

        // verify that the returned page has the correct number of elements
        assertEquals(2, result.getContent().size());

        // verify that the returned page contains the correct MobileStationResponseDto objects
        assertEquals(
                new MobileStationResponseDto(1L, 10.0f, 20.0f, 5.0f, 0, "No error"),
                result.getContent().get(0));
        assertEquals(
                new MobileStationResponseDto(2L, 30.0f, 40.0f, 5.0f, 0, "No error"),
                result.getContent().get(1));
    }

    @Test
    @DisplayName("Should not throw any exception when the mobile station with the given id exists")
    void checkIfMobileStationExistsWhenIdExists() {
        Long id = 1L;
        when(repository.existsById(id)).thenReturn(true);

        ReflectionTestUtils.invokeMethod(service, "checkIfMobileStationExists", id);

        verify(repository, times(1)).existsById(id);
    }

    @Test
    @DisplayName("Should not throw an exception when the mobile station exists")
    void checkIfMobileStationExistsWhenMobileStationExists() {
        Long id = 1L;

        when(repository.existsById(id)).thenReturn(true);

        ReflectionTestUtils.invokeMethod(service, "checkIfMobileStationExists", id);

        verify(repository, times(1)).existsById(id);
    }

    @Test
    @DisplayName("Should throw a NotFoundException when the mobile station does not exist")
    void checkIfMobileStationExistsWhenMobileStationDoesNotExistThenThrowNotFoundException() {
        Long id = 1L;
        when(repository.existsById(id)).thenReturn(false);

        assertThrows(
                NotFoundException.class,
                () -> ReflectionTestUtils.invokeMethod(service, "checkIfMobileStationExists", id));

        verify(repository, times(1)).existsById(id);
        verifyNoMoreInteractions(repository);
    }

    @Test
    @DisplayName("Should delete the mobile station by id without throwing an exception")
    void deleteByIdSilently() {
        Long id = 1L;
        doNothing().when(repository).deleteById(id);
        service.deleteByIdSilently(id);
        verify(repository, times(1)).deleteById(id);
    }

    @Test
    @DisplayName(
            "Should throw NotFoundException when the mobile station with the given id does not exist")
    void deleteByIdWhenMobileStationDoesNotExistThenThrowNotFoundException() {
        Long id = 1L;

        assertThrows(NotFoundException.class, () -> service.deleteById(id));
        verify(repository, times(1)).existsById(id);
        verify(mobileStationMovedEvent, times(0)).publishEvent(id, 0f, 0f);
        verify(repository, times(0)).deleteById(id);
    }

    @Test
    @DisplayName("Should delete the mobile station by id when it exists")
    void deleteByIdWhenMobileStationExists() {
        Long id = 1L;
        when(repository.existsById(id)).thenReturn(true);

        service.deleteById(id);

        verify(repository, times(0)).deleteById(id);
        verify(mobileStationOfflineEvent, times(1)).publishEvent(id);
        verifyNoMoreInteractions(repository, mobileStationOfflineEvent);
    }

    @Test
    @DisplayName("Should throw NotFoundException when the mobile station is not found by id")
    void findByIdWhenMobileStationNotFoundThenThrowException() {
        Long id = 1L;
        when(repository.findById(id)).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> service.findById(id));

        verify(repository, times(1)).findById(id);
    }

    @Test
    @DisplayName(
            "Should return the MobileStationResponseDto when the mobile station is found by id")
    void findByIdWhenMobileStationExists() {
        Long id = 1L;
        MobileStation mobileStation = new MobileStation(id, 40.0f, 50.0f, 10.0f, 0, "No error");
        MobileStationResponseDto expectedDto =
                new MobileStationResponseDto(id, 40.0f, 50.0f, 10.0f, 0, "No error");

        when(repository.findById(id)).thenReturn(Optional.of(mobileStation));
        when(modelMapper.map(mobileStation, MobileStationResponseDto.class))
                .thenReturn(expectedDto);

        MobileStationResponseDto actualDto = service.findById(id);

        assertEquals(expectedDto, actualDto);
        verify(repository, times(1)).findById(id);
        verify(modelMapper, times(1)).map(mobileStation, MobileStationResponseDto.class);
    }

    @Test
    @DisplayName("Should throw NotFoundException when the id does not exist")
    void updateMobileStationWhenIdDoesNotExistThenThrowNotFoundException() {
        Long id = 1L;
        MobileStationRequestDto requestDto =
                new MobileStationRequestDto(10.0f, 20.0f, 30.0f, 404, "Not Found");
        when(repository.existsById(id)).thenReturn(false);

        assertThrows(NotFoundException.class, () -> service.update(id, requestDto));
        verify(repository, times(1)).existsById(id);
        verifyNoMoreInteractions(repository);
        verifyNoInteractions(modelMapper);
        verifyNoInteractions(mobileStationMovedEvent);
    }

    @Test
    @DisplayName("Should update the mobile station when the id exists")
    void updateMobileStationWhenIdExists() {
        Long id = 1L;
        MobileStationRequestDto requestDto =
                new MobileStationRequestDto(10.0f, 20.0f, 30.0f, 40, "Error message");
        MobileStation mobileStation =
                new MobileStation(
                        id,
                        requestDto.getLastKnownX(),
                        requestDto.getLastKnownY(),
                        requestDto.getErrorRadius(),
                        requestDto.getErrorCode(),
                        requestDto.getErrorMessage());
        MobileStationResponseDto expectedResponseDto =
                new MobileStationResponseDto(
                        id,
                        requestDto.getLastKnownX(),
                        requestDto.getLastKnownY(),
                        requestDto.getErrorRadius(),
                        requestDto.getErrorCode(),
                        requestDto.getErrorMessage());

        when(repository.existsById(id)).thenReturn(true);
        when(modelMapper.map(requestDto, MobileStation.class)).thenReturn(mobileStation);
        when(repository.save(mobileStation)).thenReturn(mobileStation);
        when(modelMapper.map(mobileStation, MobileStationResponseDto.class))
                .thenReturn(expectedResponseDto);

        MobileStationResponseDto actualResponseDto = service.update(id, requestDto);

        assertEquals(expectedResponseDto, actualResponseDto);
        verify(repository, times(1)).existsById(id);
        verify(modelMapper, times(1)).map(requestDto, MobileStation.class);
        verify(repository, times(1)).save(mobileStation);
        verify(modelMapper, times(1)).map(mobileStation, MobileStationResponseDto.class);
    }

    @Test
    @DisplayName("Should save all mobile stations in the repository")
    void saveAllMobileStations() {
        List<MobileStation> mobileStations = new ArrayList<>();
        mobileStations.add(new MobileStation(1L, 10.0f, 20.0f, 5.0f, 0, ""));
        mobileStations.add(new MobileStation(2L, 30.0f, 40.0f, 5.0f, 0, ""));
        mobileStations.add(new MobileStation(3L, 50.0f, 60.0f, 5.0f, 0, ""));

        when(repository.saveAll(mobileStations)).thenReturn(mobileStations);

        service.saveAll(mobileStations);

        verify(repository, times(1)).saveAll(mobileStations);
        verifyNoMoreInteractions(repository);
    }

    @Test
    @DisplayName("Should save the mobile station and return the saved mobile station response DTO")
    void saveMobileStationAndReturnSavedMobileStationResponseDto() {
        MobileStationRequestDto requestDto =
                new MobileStationRequestDto(10.0f, 20.0f, 5.0f, 1, "Error message");
        MobileStation mobileStation =
                new MobileStation(
                        1L,
                        requestDto.getLastKnownX(),
                        requestDto.getLastKnownY(),
                        requestDto.getErrorRadius(),
                        requestDto.getErrorCode(),
                        requestDto.getErrorMessage());
        MobileStationResponseDto expectedDto =
                new MobileStationResponseDto(
                        mobileStation.getId(),
                        mobileStation.getLastKnownX(),
                        mobileStation.getLastKnownY(),
                        mobileStation.getErrorRadius(),
                        mobileStation.getErrorCode(),
                        mobileStation.getErrorMessage());

        when(modelMapper.map(requestDto, MobileStation.class)).thenReturn(mobileStation);
        when(repository.save(mobileStation)).thenReturn(mobileStation);
        when(modelMapper.map(mobileStation, MobileStationResponseDto.class))
                .thenReturn(expectedDto);

        MobileStationResponseDto actualDto = service.save(requestDto);

        verify(modelMapper, times(1)).map(requestDto, MobileStation.class);
        verify(repository, times(1)).save(mobileStation);
        verify(modelMapper, times(1)).map(mobileStation, MobileStationResponseDto.class);
        verify(mobileStationMovedEvent, times(1))
                .publishEvent(
                        mobileStation.getId(),
                        requestDto.getLastKnownX(),
                        requestDto.getLastKnownY());
        assertEquals(expectedDto, actualDto);
    }

    @Test
    @DisplayName("Should publish a mobile station moved event after saving the mobile station")
    void saveMobileStationAndPublishMobileStationMovedEvent() {
        MobileStationRequestDto requestDto = new MobileStationRequestDto();
        requestDto.setLastKnownX(50.0f);
        requestDto.setLastKnownY(50.0f);
        requestDto.setErrorRadius(10.0f);
        requestDto.setErrorCode(0);
        requestDto.setErrorMessage("No error");

        MobileStation mobileStation = new MobileStation();
        mobileStation.setId(1L);
        mobileStation.setLastKnownX(requestDto.getLastKnownX());
        mobileStation.setLastKnownY(requestDto.getLastKnownY());
        mobileStation.setErrorRadius(requestDto.getErrorRadius());
        mobileStation.setErrorCode(requestDto.getErrorCode());
        mobileStation.setErrorMessage(requestDto.getErrorMessage());

        MobileStationResponseDto responseDto = new MobileStationResponseDto();
        responseDto.setId(mobileStation.getId());
        responseDto.setLastKnownX(mobileStation.getLastKnownX());
        responseDto.setLastKnownY(mobileStation.getLastKnownY());
        responseDto.setErrorRadius(mobileStation.getErrorRadius());
        responseDto.setErrorCode(mobileStation.getErrorCode());
        responseDto.setErrorMessage(mobileStation.getErrorMessage());

        when(modelMapper.map(requestDto, MobileStation.class)).thenReturn(mobileStation);
        when(repository.save(mobileStation)).thenReturn(mobileStation);
        when(modelMapper.map(mobileStation, MobileStationResponseDto.class))
                .thenReturn(responseDto);

        MobileStationResponseDto savedMobileStation = service.save(requestDto);

        verify(modelMapper, times(1)).map(requestDto, MobileStation.class);
        verify(repository, times(1)).save(mobileStation);
        verify(modelMapper, times(1)).map(mobileStation, MobileStationResponseDto.class);
        verify(mobileStationMovedEvent, times(1))
                .publishEvent(
                        responseDto.getId(),
                        requestDto.getLastKnownX(),
                        requestDto.getLastKnownY());
        assertEquals(responseDto, savedMobileStation);
    }
}