package az.techlab.service;

import az.techlab.dto.request.BaseStationRequestDto;
import az.techlab.dto.response.BaseStationResponseDto;
import az.techlab.entity.BaseStation;
import az.techlab.event.BaseStationOfflineEvent;
import az.techlab.event.BaseStationOnlineEvent;
import az.techlab.exception.NotFoundException;
import az.techlab.repository.BaseStationRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("Base station service test")
class BaseStationServiceTest {

    @Mock
    BaseStationRepository repository;
    @Mock
    BaseStationOfflineEvent baseStationOfflineEvent;
    @Mock
    BaseStationOnlineEvent baseStationOnlineEvent;
    @Mock
    ModelMapper modelMapper;

    @InjectMocks
    BaseStationService service;

    @Test
    @DisplayName(
            "Should return a page of BaseStationResponseDto when findAll is called with pageable")
    void findAllWithPageableReturnsPageOfBaseStationResponseDto() { // create mock data
        // create mock data
        BaseStation baseStation1 =
                BaseStation.builder()
                        .id(1L)
                        .name("Base Station 1")
                        .x(10.0f)
                        .y(20.0f)
                        .detectionRadiusInMeters(100.0f)
                        .build();
        BaseStation baseStation2 =
                BaseStation.builder()
                        .id(2L)
                        .name("Base Station 2")
                        .x(30.0f)
                        .y(40.0f)
                        .detectionRadiusInMeters(200.0f)
                        .build();
        List<BaseStation> baseStations = Arrays.asList(baseStation1, baseStation2);

        // mock repository
        when(repository.findAll(any(Pageable.class))).thenReturn(new PageImpl<>(baseStations));

        // mock model mapper
        when(modelMapper.map(baseStation1, BaseStationResponseDto.class))
                .thenReturn(
                        BaseStationResponseDto.builder()
                                .id(1L)
                                .name("Base Station 1")
                                .x(10.0f)
                                .y(20.0f)
                                .detectionRadiusInMeters(100.0f)
                                .build());
        when(modelMapper.map(baseStation2, BaseStationResponseDto.class))
                .thenReturn(
                        BaseStationResponseDto.builder()
                                .id(2L)
                                .name("Base Station 2")
                                .x(30.0f)
                                .y(40.0f)
                                .detectionRadiusInMeters(200.0f)
                                .build());

        // call the method under test
        Page<BaseStationResponseDto> result = service.findAll(PageRequest.of(0, 10));

        // verify the result
        assertNotNull(result);
        assertEquals(2, result.getTotalElements());
        assertEquals(1, result.getTotalPages());
        assertEquals(2, result.getContent().size());
        assertEquals("Base Station 1", result.getContent().get(0).getName());
        assertEquals("Base Station 2", result.getContent().get(1).getName());

        // verify the repository method is called
        verify(repository, times(1)).findAll(any(Pageable.class));

        // verify the model mapper is called
        verify(modelMapper, times(1)).map(baseStation1, BaseStationResponseDto.class);
        verify(modelMapper, times(1)).map(baseStation2, BaseStationResponseDto.class);
    }

    @Test
    @DisplayName("Should delete the base station by id without throwing an exception")
    void deleteByIdSilently() {
        Long id = 1L;
        doNothing().when(repository).deleteById(id);
        service.deleteByIdSilently(id);
        verify(repository, times(1)).deleteById(id);
    }

    @Test
    @DisplayName(
            "Should throw NotFoundException when the base station with the given id does not exist")
    void deleteByIdWhenBaseStationDoesNotExistThenThrowNotFoundException() {
        Long id = 1L;
        when(repository.existsById(id)).thenReturn(false);

        assertThrows(
                NotFoundException.class,
                () -> service.deleteById(id),
                "Expected NotFoundException to be thrown when deleting a non-existent base station");

        verify(repository, times(1)).existsById(id);
        verifyNoMoreInteractions(
                repository, baseStationOfflineEvent, baseStationOnlineEvent, modelMapper);
    }

    @Test
    @DisplayName("Should delete the base station by id when the base station exists")
    void deleteByIdWhenBaseStationExists() {
        Long id = 1L;
        when(repository.existsById(id)).thenReturn(true);

        service.deleteById(id);

        verify(repository, times(0)).deleteById(id);
        verify(baseStationOfflineEvent, times(1)).publishEvent(id);
        verifyNoMoreInteractions(repository, baseStationOfflineEvent);
    }

    @Test
    @DisplayName("Should return all base stations")
    void findAllBaseStations() {
        BaseStation baseStation1 =
                BaseStation.builder()
                        .id(1L)
                        .name("Base Station 1")
                        .x(40.1234f)
                        .y(50.5678f)
                        .detectionRadiusInMeters(100f)
                        .build();
        BaseStation baseStation2 =
                BaseStation.builder()
                        .id(2L)
                        .name("Base Station 2")
                        .x(41.1234f)
                        .y(51.5678f)
                        .detectionRadiusInMeters(200f)
                        .build();
        when(repository.findAll()).thenReturn(Arrays.asList(baseStation1, baseStation2));
        List<BaseStation> baseStations = service.findAll();

        assertEquals(2, baseStations.size());
        assertEquals(baseStation1.getId(), baseStations.get(0).getId());
        assertEquals(baseStation1.getName(), baseStations.get(0).getName());
        assertEquals(baseStation1.getX(), baseStations.get(0).getX());
        assertEquals(baseStation1.getY(), baseStations.get(0).getY());
        assertEquals(
                baseStation1.getDetectionRadiusInMeters(),
                baseStations.get(0).getDetectionRadiusInMeters());
        assertEquals(baseStation2.getId(), baseStations.get(1).getId());
        assertEquals(baseStation2.getName(), baseStations.get(1).getName());
        assertEquals(baseStation2.getX(), baseStations.get(1).getX());
        assertEquals(baseStation2.getY(), baseStations.get(1).getY());
        assertEquals(
                baseStation2.getDetectionRadiusInMeters(),
                baseStations.get(1).getDetectionRadiusInMeters());

        verify(repository, times(1)).findAll();
    }

    @Test
    @DisplayName(
            "Should throw NotFoundException when the BaseStation with the given id does not exist")
    void findByIdWhenBaseStationDoesNotExistThenThrowNotFoundException() {
        Long id = 1L;
        when(repository.findById(id)).thenReturn(Optional.empty());

        assertThrows(NotFoundException.class, () -> service.findById(id));

        verifyNoMoreInteractions(
                repository, baseStationOfflineEvent, baseStationOnlineEvent, modelMapper);
    }

    @Test
    @DisplayName(
            "Should return the BaseStationResponseDto when the BaseStation with the given id exists")
    void findByIdWhenBaseStationExists() {
        Long id = 1L;
        BaseStation baseStation =
                BaseStation.builder()
                        .id(id)
                        .name("Test Base Station")
                        .x(10.0f)
                        .y(20.0f)
                        .detectionRadiusInMeters(30.0f)
                        .build();
        BaseStationResponseDto expectedResponseDto =
                BaseStationResponseDto.builder()
                        .id(id)
                        .name("Test Base Station")
                        .x(10.0f)
                        .y(20.0f)
                        .detectionRadiusInMeters(30.0f)
                        .build();

        when(repository.findById(id)).thenReturn(Optional.of(baseStation));
        when(modelMapper.map(baseStation, BaseStationResponseDto.class))
                .thenReturn(expectedResponseDto);

        BaseStationResponseDto actualResponseDto = service.findById(id);

        assertNotNull(actualResponseDto);
        assertEquals(expectedResponseDto, actualResponseDto);
        verify(repository, times(1)).findById(id);
        verify(modelMapper, times(1)).map(baseStation, BaseStationResponseDto.class);
    }

    @Test
    @DisplayName("Should throw NotFoundException when the id does not exist")
    void updateBaseStationWhenIdDoesNotExistThenThrowNotFoundException() {
        Long id = 1L;
        BaseStationRequestDto requestDto =
                new BaseStationRequestDto("Test Station", 10.0f, 20.0f, 30.0f);

        when(repository.existsById(id)).thenReturn(false);

        // when, then
        NotFoundException exception =
                assertThrows(
                        NotFoundException.class,
                        () -> service.update(id, requestDto));

        assertEquals("Base station with id 1 not found", exception.getMessage());
        verify(repository, times(1)).existsById(id);
        verifyNoMoreInteractions(
                repository, baseStationOfflineEvent, baseStationOnlineEvent, modelMapper);
    }

    @Test
    @DisplayName("Should update the base station when the id exists")
    void updateBaseStationWhenIdExists() {
        Long id = 1L;
        BaseStationRequestDto requestDto =
                new BaseStationRequestDto("Test Base Station", 10.0f, 20.0f, 30.0f);
        BaseStation baseStation =
                new BaseStation(id, "Test Base Station", 10.0f, 20.0f, 30.0f, null);
        BaseStation savedBaseStation =
                new BaseStation(id, "Test Base Station", 10.0f, 20.0f, 30.0f, null);
        BaseStationResponseDto expectedResponseDto =
                new BaseStationResponseDto(id, "Test Base Station", 10.0f, 20.0f, 30.0f, null);

        when(repository.existsById(id)).thenReturn(true);
        when(modelMapper.map(requestDto, BaseStation.class)).thenReturn(baseStation);
        when(repository.save(baseStation)).thenReturn(savedBaseStation);
        when(modelMapper.map(savedBaseStation, BaseStationResponseDto.class))
                .thenReturn(expectedResponseDto);

        BaseStationResponseDto actualResponseDto = service.update(id, requestDto);

        assertNotNull(actualResponseDto);
        assertEquals(expectedResponseDto, actualResponseDto);
        verify(repository, times(1)).existsById(id);
        verify(modelMapper, times(1)).map(requestDto, BaseStation.class);
        verify(repository, times(1)).save(baseStation);
        verify(modelMapper, times(1)).map(savedBaseStation, BaseStationResponseDto.class);
    }

    @Test
    @DisplayName("Should publish a base station online event after saving the base station")
    void saveBaseStationAndPublishBaseStationOnlineEvent() {
        BaseStationRequestDto requestDto =
                new BaseStationRequestDto("Test Base Station", 10.0f, 20.0f, 30.0f);
        BaseStation baseStation =
                new BaseStation(1L, "Test Base Station", 10.0f, 20.0f, 30.0f, null);
        BaseStation savedBaseStation =
                new BaseStation(1L, "Test Base Station", 10.0f, 20.0f, 30.0f, null);
        BaseStationResponseDto expectedResponseDto =
                new BaseStationResponseDto(1L, "Test Base Station", 10.0f, 20.0f, 30.0f, null);

        when(modelMapper.map(requestDto, BaseStation.class)).thenReturn(baseStation);
        when(repository.save(baseStation)).thenReturn(savedBaseStation);
        when(modelMapper.map(savedBaseStation, BaseStationResponseDto.class))
                .thenReturn(expectedResponseDto);

        BaseStationResponseDto actualResponseDto = service.save(requestDto);

        verify(modelMapper, times(1)).map(requestDto, BaseStation.class);
        verify(repository, times(1)).save(baseStation);
        verify(modelMapper, times(1)).map(savedBaseStation, BaseStationResponseDto.class);
        verify(baseStationOnlineEvent, times(1))
                .publishEvent(
                        savedBaseStation.getId(), savedBaseStation.getX(),
                        savedBaseStation.getY(), savedBaseStation.getDetectionRadiusInMeters());
        assertEquals(expectedResponseDto, actualResponseDto);
    }

    @Test
    @DisplayName("Should save the base station and return the saved base station response DTO")
    void saveBaseStationAndReturnSavedBaseStationResponseDto() {
        BaseStationRequestDto requestDto =
                new BaseStationRequestDto("Base Station 1", 10.0f, 20.0f, 30.0f);
        BaseStation baseStation = new BaseStation(1L, "Base Station 1", 10.0f, 20.0f, 30.0f, null);
        BaseStation savedBaseStation =
                new BaseStation(1L, "Base Station 1", 10.0f, 20.0f, 30.0f, null);
        BaseStationResponseDto expectedResponseDto =
                new BaseStationResponseDto(1L, "Base Station 1", 10.0f, 20.0f, 30.0f, null);

        when(modelMapper.map(requestDto, BaseStation.class)).thenReturn(baseStation);
        when(repository.save(baseStation)).thenReturn(savedBaseStation);
        when(modelMapper.map(savedBaseStation, BaseStationResponseDto.class))
                .thenReturn(expectedResponseDto);

        BaseStationResponseDto actualResponseDto = service.save(requestDto);

        assertNotNull(actualResponseDto);
        assertEquals(expectedResponseDto, actualResponseDto);
        verify(modelMapper, times(1)).map(requestDto, BaseStation.class);
        verify(repository, times(1)).save(baseStation);
        verify(modelMapper, times(1)).map(savedBaseStation, BaseStationResponseDto.class);
        verify(baseStationOnlineEvent, times(1))
                .publishEvent(
                        savedBaseStation.getId(), savedBaseStation.getX(),
                        savedBaseStation.getY(), savedBaseStation.getDetectionRadiusInMeters());
    }
}