package az.techlab.service;

import az.techlab.entity.MobileStation;
import az.techlab.entity.MobileStationMovement;
import az.techlab.repository.MobileStationMovementRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@DisplayName("Mobile station movement service")
class MobileStationMovementServiceTest {
    @Mock
    MobileStationMovementRepository repository;

    @InjectMocks
    MobileStationMovementService service;

    @Test
    @DisplayName("Should return an empty list when base station id is not found")
    void findByBaseStationIdWhenIdIsNotFound() {
        Long baseStationId = 1L;
        when(repository.findByBaseStationId(baseStationId)).thenReturn(new ArrayList<>());

        List<MobileStationMovement> mobileStationMovements =
                service.findByBaseStationId(baseStationId);

        assertTrue(mobileStationMovements.isEmpty());
        verify(repository, times(1)).findByBaseStationId(baseStationId);
    }

    @Test
    @DisplayName("Should return a list of mobile station movements when base station id is found")
    void findByBaseStationIdWhenIdIsFound() {
        Long baseStationId = 1L;
        // Create a mock list of MobileStationMovement objects
        List<MobileStationMovement> mobileStationMovements = new ArrayList<>();
        MobileStation mobileStation = new MobileStation(1L, 10.0f, 20.0f, 5.0f, 0, "");
        MobileStationMovement mobileStationMovement =
                new MobileStationMovement(
                        1L, mobileStation, null, 10.0f, 20.0f, 5.0f, 0, "", Instant.now(), true);
        mobileStationMovements.add(mobileStationMovement);

        // Mock the repository method to return the mock list of MobileStationMovement objects
        when(repository.findByBaseStationId(baseStationId)).thenReturn(mobileStationMovements);

        // Call the service method and verify the result
        List<MobileStationMovement> result = service.findByBaseStationId(baseStationId);
        assertEquals(mobileStationMovements, result);

        // Verify that the repository method was called once with the correct argument
        verify(repository, times(1)).findByBaseStationId(baseStationId);
    }

    @Test
    @DisplayName("Should delete all mobile station movements by base station id")
    void deleteAllByBaseStationId() {
        Long baseStationId = 1L;
        service.deleteAllByBaseStationId(baseStationId);
        verify(repository, times(1)).deleteAllByBaseStationId(baseStationId);
        verifyNoMoreInteractions(repository);
    }

    @Test
    @DisplayName("Should delete all mobile station movements by mobile station id")
    void deleteAllByMobileStationId() {
        Long mobileStationId = 1L;
        service.deleteAllByMobileStationId(mobileStationId);

        verify(repository, times(1)).deleteAllByMobileStationId(mobileStationId);
        verifyNoMoreInteractions(repository);
    }

    @Test
    @DisplayName(
            "Should save the mobile station movement and delete previous movements with the same mobile station id")
    void
    saveMobileStationMovementAndDeletePreviousMovements() { // Create a
        // MobileStationMovement object
        MobileStation mobileStation =
                MobileStation.builder()
                        .id(1L)
                        .lastKnownX(40.1234f)
                        .lastKnownY(50.5678f)
                        .errorRadius(0.5f)
                        .errorCode(0)
                        .errorMessage("No error")
                        .build();

        MobileStationMovement mobileStationMovement =
                MobileStationMovement.builder()
                        .id(1L)
                        .mobileStation(mobileStation)
                        .x(40.1234f)
                        .y(50.5678f)
                        .errorRadius(0.5f)
                        .errorCode(0)
                        .errorMessage("No error")
                        .createdAt(Instant.now())
                        .isActive(true)
                        .build();

        // Mock the repository method calls
        when(repository.save(mobileStationMovement)).thenReturn(mobileStationMovement);

        // Call the service method
        service.save(mobileStationMovement);

        // Verify the repository method calls
        verify(repository, times(1)).deleteAllByMobileStationId(mobileStation.getId());
        verify(repository, times(1)).save(mobileStationMovement);
    }
}