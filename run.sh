#!/bin/bash

# Build the backend project
./gradlew clean && ./gradlew bootJar

# Start the backend using Docker Compose
docker-compose up -d

# Navigate to the frontend folder
# shellcheck disable=SC2164
cd front-end

# Install dependencies for the frontend
npm install

printf "Sleeping for more 5 seconds to wait for the backend to start\n"
# Wait for the backend to start
sleep 5
# Start the frontend
npm start
