FROM alpine:latest
RUN apk add openjdk8
COPY build/libs/in-house-nav-system-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app/
ENTRYPOINT ["java"]
CMD ["-jar", "/app/in-house-nav-system-0.0.1-SNAPSHOT.jar"]
