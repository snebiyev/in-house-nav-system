import React from 'react';
import './App.css';
import Header from './components/page/Header';
import BaseStationList from './components/page/BaseStationList';

import MobileStationList from './components/page/MobileStationList';
import Message from './components/page/Message';

function App() {
  console.log(`App called`);

  return (
    <div className='container mt-1'>
      <Header />
      <hr />
      <Message />
      <BaseStationList />
      <hr />
      <MobileStationList />
    </div>
  );
}

export default App;
