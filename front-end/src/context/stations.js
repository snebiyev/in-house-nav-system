import React, { createContext, useContext, useEffect, useState } from 'react';
import axios from 'axios';

const StationsContext = createContext([]);

export function useStations() {
  return useContext(StationsContext);
}

export function StationsProvider({ children }) {
  const [baseStations, setBaseStations] = useState([]);
  const [mobileStations, setMobileStations] = useState([]);
  const [error, setError] = useState('');
  const [msg, setMsg] = useState('');
  const apiURL = 'http://localhost:8081/api';

  useEffect(() => {
    fetchBaseStations();
    fetchMobileStations();
  }, []);

  async function fetchBaseStations() {
    console.log(`fetchBaseStations called`);
    await axios
      .get(`${apiURL}/base-stations/?page=0&size=100&sort=asc`)
      .then((response) => {
        setBaseStations(response.data.content);
        // setMsg('Base stations loaded.');
      })
      .catch((error) => {
        console.error(error);
        setError('Something went wrong. Please check the console.');
      });
  }

  async function fetchMobileStations() {
    console.log(`fetchMobileStations called`);
    await axios
      .get(`${apiURL}/mobile-stations/?page=0&size=100&sort=asc`)
      .then((response) => {
        setMobileStations(response.data.content);
        // setMsg('Mobile stations loaded.');
      })
      .catch((error) => {
        console.error(error);
        setError('Something went wrong. Please check the console.');
      });
  }

  function addBaseStation(baseStation) {
    axios
      .post(`${apiURL}/base-stations/`, baseStation)
      .then((response) => {
        setMsg('Base station added.');
      })
      .then(() => {
        fetchBaseStations();
        fetchMobileStations();
      })
      .catch((error) => {
        console.error(error);
        setError('Something went wrong. Please check the console.');
      });
  }

  function addMobileStation(mobileStation) {
    axios
      .post(`${apiURL}/mobile-stations/`, mobileStation)
      .then((response) => {
        setMsg('Mobile station added.');
      })
      .then(() => {
        fetchBaseStations();
        fetchMobileStations();
      })
      .catch((error) => {
        console.error(error);
        setError('Something went wrong. Please check the console.');
      });
  }

  function deleteBaseStation(id) {
    axios
      .delete(`${apiURL}/base-stations/${id}`)
      .then((response) => {
        // Remove the deleted base station from the list of base stations
        // @ts-ignore
        setBaseStations((baseStations) =>
          baseStations.filter((baseStation) => baseStation.id !== id)
        );
        setMsg('Base station deleted.');
      })
      .then(() => {
        fetchMobileStations();
      })
      .catch((error) => {
        console.error(error);
        setError('Something went wrong. Please check the console.');
      });
  }

  function deleteMobileStation(id) {
    axios
      .delete(`${apiURL}/mobile-stations/${id}`)
      .then((response) => {
        // Remove the deleted base station from the list of base stations
        setMobileStations((mobileStations) =>
          mobileStations.filter((mobileStation) => mobileStation.id !== id)
        );
        setMsg('Mobile station deleted.');
      })
      .then(() => {
        fetchBaseStations();
      })
      .catch((error) => {
        console.error(error);
        setError('Something went wrong. Please check the console.');
      });
  }

  function moveMobileStation(id, x, y) {
    axios
      .put(`${apiURL}/mobile-stations/${id}/move/`, {
        lastKnownX: x,
        lastKnownY: y,
      })
      .then((response) => {
        setMsg('Mobile station moved.');
        fetchBaseStations();
        fetchMobileStations();
      })
      .catch((error) => {
        console.error(error);
        setError('Something went wrong. Please check the console.');
      });
  }

  const shared = {
    baseStations,
    mobileStations,
    addBaseStation,
    addMobileStation,
    deleteBaseStation,
    deleteMobileStation,
    moveMobileStation,
    error,
    msg,
    setError,
    setMsg,
  };

  return (
    <StationsContext.Provider value={shared}>
      {children}
    </StationsContext.Provider>
  );
}
