import React, {useState} from 'react';
import {useStations} from '../../context/stations';

export default function MobileStation({
  id,
  lastKnownX,
  lastKnownY,
  errorRadius,
  errorCode,
  errorMessage,
}) {
  const { deleteMobileStation, moveMobileStation } = useStations();
  const [x, setX] = useState(lastKnownX);
  const [y, setY] = useState(lastKnownY);
  const style = errorCode ? 'bg-danger' : 'bg-primary';

  return (
    <div className='col-sm-3'>
      <div className={`card text-white ${style} mb-3`}>
        <div className='card-header'>
          #{id}
          <button
            className='btn btn-warning btn-sm float-right'
            onClick={() => deleteMobileStation(id)}
          >
            Delete
          </button>
        </div>

        <div className='card-body'>
          <ul className='list-group'>
            <li className='list-group-item list-group-item-action list-group-item-primary'>
              Last known X: {lastKnownX}
            </li>
            <li className='list-group-item list-group-item-action list-group-item-primary'>
              Last known Y: {lastKnownY}
            </li>
            {errorCode && (
              <>
                <li className='list-group-item list-group-item-action list-group-item-primary'>
                  Error radius: {errorRadius}
                </li>
                <li className='list-group-item list-group-item-action list-group-item-primary'>
                  Error code: {errorCode}
                </li>
                <li className='list-group-item list-group-item-action list-group-item-primary'>
                  <i>{errorMessage}</i>
                </li>
              </>
            )}
            <li className='list-group-item list-group-item-action list-group-item-primary'>
              <form
                className='form-inline'
                onSubmit={(e) => {
                  e.preventDefault();
                  moveMobileStation(id, x, y);
                }}
              >
                <div className='form-group mb-2 mr-2'>
                  <label htmlFor='x'>X:</label>
                  <input
                    type='text'
                    className='form-control form-control-sm ml-1'
                    style={{ width: '55px' }}
                    id='x'
                    value={x}
                    autoComplete='off'
                    onChange={(e) => {
                      setX(e.target.value);
                    }}
                  />
                </div>
                <div className='form-group mb-2 ml-2'>
                  <label htmlFor='y'>Y:</label>
                  <input
                    type='text'
                    className='form-control form-control-sm ml-1'
                    style={{ width: '55px' }}
                    id='y'
                    value={y}
                    autoComplete='off'
                    onChange={(e) => {
                      setY(e.target.value);
                    }}
                  />
                </div>
              </form>
              <button
                className='btn btn-success btn-sm btn-block'
                onClick={() => moveMobileStation(id, x, y)}
              >
                Move
              </button>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
