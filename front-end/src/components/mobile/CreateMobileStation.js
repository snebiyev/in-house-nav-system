import React, {useState} from 'react';
import {useStations} from '../../context/stations';

export default function CreateMobileStation() {
  const { addMobileStation } = useStations();
  const [formValues, setFormValues] = useState({
    lastKnownX: '',
    lastKnownY: '',
  });
  const handleChange = (event) => {
    const { id, value, type, checked } = event.target;

    setFormValues((prevValues) => {
      if (type === 'checkbox') {
        return {
          ...prevValues,
          [id]: checked,
        };
      } else {
        return {
          ...prevValues,
          [id]: value,
        };
      }
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log('Form submitted:', formValues);
    addMobileStation(formValues);
    setFormValues({
      lastKnownX: '',
      lastKnownY: '',
    });
  };

  return (
    <div className='row'>
      <div className='col'>
        <form onSubmit={handleSubmit}>
          <div className='form-group'>
            <input
              type='text'
              className='form-control mb-1'
              id='lastKnownX'
              placeholder='Enter x coordinate'
              value={formValues.lastKnownX}
              onChange={handleChange}
            />
            <input
              type='text'
              className='form-control  mb-1'
              id='lastKnownY'
              placeholder='Enter y coordinate'
              value={formValues.lastKnownY}
              onChange={handleChange}
            />
          </div>
          <button type='submit' className='btn btn-primary'>
            Add Mobile Station
          </button>
        </form>
      </div>
    </div>
  );
}
