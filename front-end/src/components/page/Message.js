import React, {useEffect} from 'react';
import {useStations} from '../../context/stations';

export default function Message() {
  console.log(`Message called`);
  // @ts-ignore
  const { error, setError, msg, setMsg } = useStations();
  useEffect(() => {
    console.log(`error: ${error}`);
    console.log(`msg: ${msg}`);
    if (error) {
      setTimeout(() => {
        setError('');
      }, 5000);
    }
    if (msg !== '') {
      setTimeout(() => {
        setMsg('');
      }, 5000);
    }
  }, [error, msg, setError, setMsg]);
  return (
    <>
      {error && <div className='alert alert-danger'>{error}</div>}
      {msg && <div className='alert alert-success'>{msg}</div>}
    </>
  );
}
