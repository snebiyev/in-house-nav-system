import React from 'react';
import CreateBaseStation from '../base/CreateBaseStation';
import CreateMobileStation from '../mobile/CreateMobileStation';

export default function Header() {
  const [showBS, setShowBS] = React.useState(false);
  const [showMS, setShowMS] = React.useState(false);

  const handleBS = () => {
    setShowBS(!showBS);
    setShowMS(false);
  };
  const handleMS = () => {
    setShowMS(!showMS);
    setShowBS(false);
  };

  return (
    <>
      <div className='row'>
        <div className='col float-left'>
          <h1>React Station App</h1>
        </div>
        <div className='col text-right'>
          <button className='btn btn-primary mr-2' onClick={handleBS}>
            Add Base Station {showBS && <span aria-hidden='true'>&times;</span>}
          </button>
          <button className='btn btn-primary' onClick={handleMS}>
            Add Mobile Station{' '}
            {showMS && <span aria-hidden='true'>&times;</span>}
          </button>
        </div>
      </div>
      {showBS && <CreateBaseStation />}
      {showMS && <CreateMobileStation />}
    </>
  );
}
