import React from 'react';
import {useStations} from '../../context/stations';
import MobileStation from '../mobile/MobileStation';

export default function MobileStationList() {
  const { mobileStations } = useStations();

  const renderedMobileStations = mobileStations.map((mobileStation) => (
    <MobileStation key={mobileStation.id} {...mobileStation} />
  ));

  return (
    <div className='row'>
      <div className='col'>
        <h2>Mobile Stations</h2>
        {renderedMobileStations ? (
          <div className='row'>{renderedMobileStations}</div>
        ) : (
          <div className='alert alert-info'>No mobile stations found.</div>
        )}
      </div>
    </div>
  );
}
