import React from 'react';
import {useStations} from '../../context/stations';
import BaseStation from '../base/BaseStation';

export default function BaseStationList() {
  const { baseStations } = useStations();

  const renderedBaseStations = baseStations.map((baseStation) => (
    <BaseStation key={baseStation.id} {...baseStation} />
  ));

  return (
    <div className='row'>
      <div className='col'>
        <h2>Base Stations</h2>
        {renderedBaseStations ? (
          <div className='row'>{renderedBaseStations}</div>
        ) : (
          <div className='alert alert-info'>No base stations found.</div>
        )}
      </div>
    </div>
  );
}
