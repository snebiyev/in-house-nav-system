import React, {useState} from 'react';
import {useStations} from '../../context/stations';

export default function CreateBaseStation() {
  const { addBaseStation } = useStations();
  const [formValues, setFormValues] = useState({
    name: '',
    x: '',
    y: '',
    detectionRadiusInMeters: '',
  });
  const handleChange = (event) => {
    const { id, value, type, checked } = event.target;

    setFormValues((prevValues) => {
      if (type === 'checkbox') {
        return {
          ...prevValues,
          [id]: checked,
        };
      } else {
        return {
          ...prevValues,
          [id]: value,
        };
      }
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log('Form submitted:', formValues);
    addBaseStation(formValues);
    setFormValues({
      name: '',
      x: '',
      y: '',
      detectionRadiusInMeters: '',
    });
  };

  return (
    <div className='row'>
      <div className='col'>
        <form onSubmit={handleSubmit}>
          <div className='form-group'>
            <input
              type='text'
              className='form-control  mb-1'
              placeholder='Enter Base Station Name'
              id='name'
              value={formValues.name}
              onChange={handleChange}
            />
            <input
              type='text'
              className='form-control  mb-1'
              id='x'
              placeholder='Enter x coordinate'
              value={formValues.x}
              onChange={handleChange}
            />
            <input
              type='text'
              className='form-control  mb-1'
              id='y'
              placeholder='Enter y coordinate'
              value={formValues.y}
              onChange={handleChange}
            />
            <input
              type='text'
              className='form-control'
              id='detectionRadiusInMeters'
              placeholder='Enter detection radius in meters'
              value={formValues.detectionRadiusInMeters}
              onChange={handleChange}
            />
          </div>
          <button type='submit' className='btn btn-primary'>
            Add Base Station
          </button>
        </form>
      </div>
    </div>
  );
}
