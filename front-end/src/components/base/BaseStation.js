import React from 'react';
import { useStations } from '../../context/stations';
import { v4 as uuid } from 'uuid';

export default function BaseStation({
  name,
  id,
  x,
  y,
  detectionRadiusInMeters,
  reports,
}) {
  const { deleteBaseStation } = useStations();
  const handleDelete = (id) => {
    deleteBaseStation(id);
  };

  return (
    <div className='col-sm-3'>
      <div className='card text-white bg-info mb-3'>
        <div className='card-header'>
          {name}
          <button
            className='btn btn-warning btn-sm float-right'
            onClick={() => handleDelete(id)}
          >
            Delete
          </button>
        </div>

        <div className='card-body'>
          <ul className='list-group'>
            <li className='list-group-item list-group-item-info'>
              X coordinate: {x}
            </li>
            <li className='list-group-item list-group-item-info'>
              Y coordinate: {y}
            </li>
            <li className='list-group-item list-group-item-info'>
              Detection radius: {detectionRadiusInMeters}
            </li>
          </ul>
          {reports && reports.length > 0 ? (
            reports.map((report) => (
              <button
                type='button'
                className='btn btn-primary ml-0 mr-2 mt-2'
                key={uuid()}
              >
                <small>MS</small>{' '}
                <span className='badge badge-light'>
                  #{report.mobileStation.id}
                </span>
                <span className='sr-only'>mobile station</span>
              </button>
            ))
          ) : (
            <></>
          )}
        </div>
      </div>
    </div>
  );
}
